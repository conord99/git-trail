### Tkinter to Create Basic Window ###

from tkinter import *

from PIL import Image, ImageTk

class Window(Frame):
    def __init__(self, master = None):
        Frame.__init__(self, master) # Referencing initialization, Frame imported from tkinter
        self.master = master # Calling this the master widget
        self.init_window() # Call init_window function

    def init_window(self):
        self.master.title("GUI")
        self.pack(fill=BOTH,expand=1)

        # Button Example
        # quitButton = Button(self, text="Quit", command=self.client_exit) # Command (Event Handling) calling client exit function
        # quitButton.place(x=0,y=0)

        # Define menu and where it goes
        menu = Menu(self.master)
        self.master.config(menu=menu)
        
        # File tab > Cascade > Exit
        file = Menu(menu)
        file.add_command(label='Save') # Could add command=self.client_save, calls client_save function
        file.add_command(label='Exit', command=self.client_exit) # Calls client_exit function
        menu.add_cascade(label='File', menu=file) # Add file tab to menu

        # Edit tab > Cascade > Undo
        edit = Menu(menu)
        edit.add_command(label='Undo')
        edit.add_command(label='Show Image', command=self.showImg()) # () => Calling function in the initialization
        edit.add_command(label='Show Text', command=self.showTxt)
        menu.add_cascade(label='Edit', menu=edit) # Add edit tab to menu


    def showImg(self):
        load = Image.open('fusion.PNG')
        render = ImageTk.PhotoImage(load)
        img = Label(self, image=render)
        img.image = render
        img.place(x=0, y=0)

    def showTxt(self):
        text = Label(self, text='Sensor Fusion')
        text.pack()


    def client_exit(self):
        exit() # Built in python function



root = Tk() #importing Tk() from tkinter, root window
root.geometry("400x300")

app = Window(root)

root.mainloop()


