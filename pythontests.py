### Numpy ###
# Optimizes speed of python with big data matrices

import numpy as np

### Example 1:
height=[1,2,3,4]
weight=[5,6,7,8]

np_height=np.array(height)
np_height

np_weight=np.array(weight)
np_weight

bmi=np_weight/np_height**2
print(bmi)

print(bmi[1])
print(bmi > 1)
print(bmi[bmi>1])

### Example 2:
# height list
height_in=[60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78]

# Import numpy (already done above)

# Create a numpy array from height_in: np_height_in
np_height_in = np.array(height_in)

# Print out np_height_in
print(np_height_in)

# Convert np_height_in to m: np_height_m
np_height_m = np_height_in*0.0254

# Print np_height_m
print(np_height_m)


### 2D Numpy Arrays (Lists of Lists)
np_2d = np.array([[1, 2, 3, 4], [5, 6, 7, 8]])
print(np_2d)
print(np_2d.shape) # 2 rows, 5 columns
print(np_2d[0][2])
print(np_2d[0,2])
print(np_2d[:,1:3]) # Note: 3 index not included, only 1 and 2

### Example 3:
# Import numpy package (already done above)
baseball = ([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
updated = ([[0.5, 0.75, 1], [0.5, 0.75, 1], [0.5, 0.75, 1]])
# Create np_baseball (3 cols)
np_baseball = np.array(baseball)

# Print out addition of np_baseball and updated
print(np_baseball+updated)

# Create numpy array: conversion
conversion = [0.0254, 0.453592, 1]

# Print out product of np_baseball and conversion
print(np_baseball*conversion)


### Numpy Basic Stats

# Example: City height and weight data
np_city = np.array([[1, 2], [3, 4], [5, 6]])
print(np_city)
print(np_city.shape)
# Mean and Median height
avg_height = np.mean(np_city[:, 0])
med_height = np.median(np_city[:, 0])
print(avg_height)
print(med_height)
# Standard Dev
std_height = np.std(np_city[:,0])
print(std_height)

# Example: Soccer player positions and heights
# heights and positions are available as lists

# Import numpy

# Convert positions and heights to numpy arrays: np_positions, np_heights
positions = ['GK','D','M','A']
heights = [191, 184, 185, 180]

np_positions = np.array(positions)
np_heights = np.array(heights)

# Heights of the goalkeepers: gk_heights
gk_heights = np_heights[np_positions == 'GK']

# Heights of the other players: other_heights
other_heights = np_heights[np_positions != 'GK']

# Print out the median height of goalkeepers. Replace 'None'
print("Median height of goalkeepers: " + str(np.median(gk_heights)))

# Print out the median height of other players. Replace 'None'
print("Median height of other players: " + str(np.median(other_heights)))



